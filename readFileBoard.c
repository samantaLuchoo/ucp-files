#include <stdio.h>
#include <stdlib.h>
#include "declarations.h"

void readFileBoard(char* argv[], int choice)
{
	char* file = argv[1];
	FILE* fileptr;
	int length = 0;
	int width = 0;
	char* location = (char*)malloc(sizeof(char));
	char* locationRow;
	char* locationColumn;
	struct Node* headptr = NULL;
	int row = 0;
	int column = 0;
	char direction;
	int shiplngth = 0;
	char* shipName = (char*)malloc(sizeof(char));
	char** board = (char**)malloc(length*sizeof(char*));

	fileptr = fopen(file, "r");

	if (fileptr == NULL)
	{
		printf("Error! There was a problem while opening file.\n");
		exit(0);
	}
	else 
	{
		

			fscanf(fileptr, "%d ", &width);
			fseek(fileptr, 1, SEEK_CUR);
			fscanf(fileptr, "%d", &length);

			if (validateLengthWidth(length,width) == FALSE )
			{
				exit(0);
			}

			createBoard(board, length, width);
			printBoard(length, width, board);

			while (*location == EOF) 
			{
				fscanf(fileptr, "%s ", location);
				fscanf(fileptr, "%c ", &direction);
				fscanf(fileptr, "%d ", &shiplngth);
				fscanf(fileptr, "%[^\n]%*c", shipName);

				printf("%s %c %d %s\n", location, direction, shiplngth, shipName);

				stringSplit(&locationRow, &locationColumn, location);
				upperCase(&direction);

				printf("%c %c\n", *locationRow, *locationColumn);
				printf("%c\n", direction);

				row = *locationRow -64;
				column = *locationColumn - 48;

				printf("%d\n%d\n", row, column);

				if (validateLocation(row, column, length, width) == FALSE)
				{
					printf("Error, location is either outside of the board or the field for location is empty.\n");
					exit(0);
				}

				if (validateDirection(direction) == FALSE)
				{
					printf("Error, wrong direction in the board file.\n");
					exit(0);
				}

				if (validateShipLength(shiplngth,length, width, direction, row, column ) == FALSE)
				{
					printf("Error, ship length caused ship to go outside board or the ship length in the board is not a positive number.\n");
					exit(0);
				}

				if (validateShipName(shipName) == FALSE)
				{
					printf("Error, ship name field is empty in the file\n");
					exit(0);
				}

				addShip(row, column, direction, shipName, shiplngth, board);
				printf("Ship added\n");

			};
		
		if (choice == 1)
		{
			GamePlayMode(length, width, shipName, argv, board);
		}

	}

	fclose(fileptr);

}
