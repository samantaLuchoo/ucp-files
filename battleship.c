#include <stdio.h>
#include "declarations.h"

int main (int argc, char* argv[])
{
	int selection = 0;
	int exit = 0;
	do
	{
		/*Stores what the user has selected from the option.*/
		menu();
		scanf("%d", &selection);

		/*if the selection is 0, the program will exit.*/
		if (selection == 0)
		{
			exit = 0;
		}
		/*if the selection is 1, game will be played */
		else if (selection == 1)
		{
			if (argc < 2)
			{
				printf("Enter the board file and the missile file after ./battleship");
			}
			else 
			{
				/*calling function to play game.*/
				readFileBoard(argv, selection);
			}
		}
		/*if the selection is 2, the linked list will be printed.*/
		else if (selection == 2)
		{
			if (argc < 2)
			{
				printf("Enter the board file and the missile file after ./battleship");
			}
			else 
			{
				/*calling function to read the missile file and print the linked list.*/
				readFileMissile(argv, selection);
			}

		}
		/*if selection is 3, the user will be able to write a board file.*/
		else if (selection == 3)
		{
			writeFile(selection);
		}
		/*if the selection is 4, the user will be able to write a missile file.*/
		else if (selection == 4)
		{
			writeFile(selection);
		}
		/*Checking for invalid input of selection.*/
		else 
		{
			printf("Error ! Invalid selection chosen.\n");
			exit = 0;
		}
	} while(selection != 0);

	return exit;

}
