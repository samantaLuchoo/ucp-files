#include <stdio.h>
#include <stdlib.h>
#include "declarations.h"

void push (struct Node *headptr, int len, char* newValue)
{
	struct Node *nodeptr = (struct Node*)malloc(sizeof(struct Node));
	int index = 0;

	nodeptr -> value = malloc(len);
	nodeptr -> nextptr = (headptr);

	for (index=0; index<len; index++)
	{
		*((char*)(nodeptr -> value+index)) = *((char*)(newValue+index));
	}
	
	printf("%s\n", nodeptr->value);
	headptr = nodeptr;
}

void createLinkedList(struct Node* headptr, char* missileType,int len)
{
	push(headptr, len, missileType);
}
