#include <stdio.h>
#include <string.h>
#include "declarations.h"

int validateMissileType(char* missileType)
{
	int validate = TRUE;
	if (missileType == NULL || *missileType == ' ')
	{
		validate = FALSE;
	}
	else if ((strcmp(missileType, "SINGLE") == 0) || (strcmp(missileType, "SPLASH") ==0) || (strcmp(missileType, "H-LINE") == 0) || (strcmp(missileType, "V-LINE") == 0)) 
	{
		validate = TRUE;
	}
	return validate;
}
