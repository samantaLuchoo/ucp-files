#include <stdio.h>
#include "declarations.h"

int validateLocation(int row, int column, int length, int width)
{
	int validate = TRUE;

	if ((row < 0) || (row > width))
	{
		validate = FALSE;
	}

	if ((column < 0) || (column > length))
	{
		validate = FALSE;
	}

	return validate;
	
}
