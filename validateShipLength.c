#include <stdio.h>
#include "declarations.h"

int validateShipLength(int shipLngth, int length, int width, char direction,int row, int column )
{
	int validate = 0;
	int calc = 0;

	switch (direction)
	{
		case 'N':
			if (shipLngth < column)
			{
				validate = 1;
			}
			break;

		case 'S':
			if (shipLngth < ((length - column)+1))
			{
				validate = 1;
			}
			break;

		case 'W':
			if (shipLngth < row)
			{
				validate = 1;
			}
			break;

		case 'E':
			calc = (width - column) +1;
			if (shipLngth <= calc)
			{
				validate = 1;
			}
			break;
		default:
			printf("Error while validating ship length, the direction is wrong.\n");
			break;
	}

	return validate;
}
