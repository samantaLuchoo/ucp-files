CC = gcc
CFLAGS = -Wall -ansi -pedantic -Werror
OBJ = battleship.c menu.c readFileBoard.c readFileMissile.c stringSplit.c validateLengthWidth.c validateDirection.c validateLocation.c validateMissileType.c validateShipLength.c validateShipName.c createBoard.c printBoard.c addShip.c GamePlayMode.c LinkedList.c upperCase.c upperCaseString.c writeFile.c 
EXEC = battleship

$(EXEC) : $(OBJ)
	$(CC) $(OBJ) -o $(EXEC)

battleship.o : battleship.c declarations.h
	$(CC) -c battleship.c $(CFLAGS)

menu.o : menu.c 
	$(CC) -c menu.c $(CFLAGS)

readFileBoard.o : readFileBoard.c declarations.h 
	$(CC) -c readFileBoard.c $(CFLAGS)

readFileMissile.o : readFileMissile.c declarations.h
	$(CC) -c readFileMissile.c $(CFLAGS)

stringSplit.o : stringSplit.c declarations.h 
	$(CC) -c stringSplit.c $(CFLAGS)

validateLengthWidth.o : validateLengthWidth.c declarations.h
	$(CC) -c validateLengthWidth.c $(CFLAGS)

validateLocation.o : validateLocation.c declarations.h
	$(CC) -c validateLocation.c $(CFLAGS)

validateDirection.o : validateDirection.c declarations.h 
	$(CC) -c validateDirection.c $(CFLAGS)

validateShipLength.o : validateShipLength.c declarations.h
	$(CC) -c valiadateShipLength.c $(CFLAGS)

validateShipName.o : validateShipName.c declarations.h
	$(CC) -c validateShipName.c $(CFLAGS)

validateMissileType.o : validateMissileType.c declarations.h 
	$(CC) -c validateMissileType.c $(CFLAGS)

upperCase.o : upperCase.c declarations.h
	$(CC) -c upperCase.c $(CFLAGS)

upperCaseString.o : upperCaseString.c declarations.h
	$(CC) -c upperCase.c $(CFLAGS)

createBoard.o : createBoard.c 
	$(CC) -c createBoard.c $(CFLAGS)

addShip.o : addShip.c declarations.h
	$(CC) -c addShip.c $(CFLAGS)

LinkedList.o : LinkedList.c declarations.h
	$(CC) -c LinkedList.c $(CFLAGS)

printBoard.o : printBoard.c declarations.h
	$(CC) -c printBoard.c $(CFLAGS)

GamePlayMode.o : GamePlayMode.c declarations.h
	$(CC) -c GamePlayMode.c $(CFLAGS)

writeFile.o : writeFile.c declarations.h
	$(CC) -c writeFile.c $(CFLAGS)

clean :
	rm -f $(EXEC) $(OBJ)

