#include <stdio.h>

void upperCase(char* element)
{
	int asciiNumber;
	
	asciiNumber = *element;

	if ((asciiNumber > 96 && asciiNumber < 123))
	{
		*element = ((char)(asciiNumber - 32));
	}
}
