#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int stringSplit (char **locationRow, char **locationColumn, char* location)
{
	int stringlength;

	stringlength = strlen(location);

	*locationRow = (char*)malloc(sizeof(char) * 2);
	*locationColumn = (char*)malloc(sizeof(char) * (stringlength-2));

	if  (locationRow == NULL || locationColumn == NULL)
	{
		printf("Error while spliting location.\n");
		exit(0);
	}

	strncpy(*locationRow, location,1);
	strncpy(*locationColumn, location+1,stringlength-1);
	return(0);




}
