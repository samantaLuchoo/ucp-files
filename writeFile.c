#include <stdio.h>
#include <stdlib.h>
#include "declarations.h"

void writeFile(int choice)
{
	char* file = (char*)malloc(sizeof(char));
	FILE* fileptr;
	int length = 0;
	int width = 0;
	char* location = (char*)malloc(sizeof(char));
	char direction;
	int shiplngth = 0;
	char* shipName = (char*)malloc(sizeof(char));
	int stop = FALSE;
	int row = 0;
	int column = 0;
	char* locationRow;
	char* locationColumn;
	char* missileType = (char*)malloc(sizeof(char));

	printf("Enter the name of the file you want to create.\n");
	scanf("%s", file);
	fileptr = fopen(file, "w");

	if (fileptr == NULL)
	{
		printf("There was an error while creating your file.\n");
		exit(0);
	}

	if (choice == 3)
	{
		do
		{
			printf("Enter the length of the board: length shouldbe between 1 and 12 inclusive.\n");
			scanf("%d", &length);

			printf("Enter the width of the board: width should be between 1 - 12 inclusive.\n");
			scanf("%d", &width);	

		} while (validateLengthWidth(length, width) == FALSE);

		fprintf(fileptr, "%d,%d", length, width); 
		
		while (stop == FALSE)
		{
			do
			{
				printf("Enter the location of the ship in the format 'D4' where 'D' represents the column and '4' the row.\n");
				scanf("%s",location);
				stringSplit(&locationRow, &locationColumn, location);
				row = *locationRow - 64;
				column = *locationColumn - 48;

			} while (validateLocation(row, column, length, width) == FALSE);
			do 
			{
				printf("Enter the direction of the ship: choose from\n N - North\n S - South\n E - East\n W - West\n");
				scanf("%c", &direction);

			} while(validateDirection(direction) == FALSE);

			do 
			{
				printf("Enter the length of the ship: the ship should be inside the board.\n");
				scanf("%d", &shiplngth);
			} while (validateShipLength(shiplngth, length, width, direction, row, column) == FALSE);

			do 
			{
				printf("Enter the name of the ship.\n");
				scanf("%s", shipName);
			} while (validateShipName(shipName) == FALSE);

			fprintf(fileptr, "%s %c %d %s\n", location, direction, shiplngth, shipName);

			printf("Do you want to add more ships? Type: \n 0 for No\n 1 for Yes\n");
			scanf("%d", &stop);
		};
		
	}
	else if (choice == 4)
	{
		while (stop == FALSE)
		{
			printf("Enter the missile type you want to lanch when playing the game. Choose from:\n Single\n Splash\n V-line\n H-line\n");
			scanf("%s", missileType);
			fprintf(fileptr,"%s\n", missileType);

			printf("Do you want to add more ships? Type: \n 0 for No\n 1 for Yes\n");
			scanf("%d", &stop);
		};
		
	}

	fclose(fileptr);
	free(missileType);
	free(shipName);
	free(location);
}

