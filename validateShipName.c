#include <stdio.h>
#include "declarations.h"

int validateShipName(char* shipName)
{
	int validate = TRUE;

	if (shipName == NULL || *shipName == ' ')
	{
		validate = FALSE;
	}

	return validate;
}
