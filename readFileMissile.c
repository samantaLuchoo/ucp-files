#include <stdio.h>
#include <stdlib.h>
#include "declarations.h"

void readFileMissile(char* argv[], int choice)
{
	char* file = argv[2];
	FILE* fileptr;
	char* missileType = (char*)malloc(sizeof(char));
	struct Node* headptr;
	int len;

	headptr = NULL;
	len = sizeof(char*);

	fileptr = fopen(file, "r");

	if (fileptr == NULL)
	{
		printf("Error while opening file.\n");
		exit(0);
	}
	else 
	{
		do 
		{
			fscanf(fileptr, "%s", missileType);
			upperCaseString(missileType);

			if (validateMissileType(missileType) == FALSE)
			{
				printf("Error in field missile in the file, please check if it is empty.\n");
				exit(0);
			}
			
			createLinkedList(headptr, missileType, len);

			if (choice == 2)
			{
				printf("%s\n", missileType);
			}

		} while (!(feof(fileptr)));
	}

	fclose(fileptr);
}
