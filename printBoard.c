#include <stdio.h>
#include "declarations.h"

void printBoard(int length, int width, char** board)
{
	int i = 0;
	int j = 0;
	int k = 0;
	char row;
	int column = 0;

	printf(" :)|");

	for (i=1; i<width+1; i++)
	{
		row = i+64;
		printf("| %c |", row);
	}
	printf("\n");

	for (i=0; i< length; i++)
	{
		column = i+1;
		printf("---");

		for (k=0; k<width; k++)
		{
			printf("++===");
		}
		printf("+\n");

		printf(" %d |", column);

		for (j=0; j<width;j++)
		{
			#ifdef MONO
				if (board[i][j] == '#')
				{
					printf("| # |");
				}
				else if (board[i][j] == 'X')
				{
					printf("| X |");
				}
				else if (board[i][j] == 'O')
				{
					printf("| O |");
				}
				else 
				{
					printf("| # |");
				}
			#else 
				if(board[i][j] == '#')
				{
					printf("| %s#%s |", BLUE, RESET);
				}
				else if (board[i][j] == 'O')
				{
					printf("| %sO%s |", GREEN, RESET);
				}
				else if (board[i][j] == 'X')
				{
					printf("| %sX%s |", RED, RESET);
				}
				else 
				{
					#ifdef DEBUG
						printf("| %s#%s |", MAGENTA, RESET);
					#else
						printf("| %s#%s |", BLUE, RESET); 
					#endif
				}
				
			#endif

		}
		printf("\n");
	} 
}
