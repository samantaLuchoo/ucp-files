#define TRUE 1
#define FALSE 0
#define RED "\033[0;32m"
#define GREEN "\033[0:32m"
#define BLUE "\033[1;3m"
#define MAGENTA "\033[1;35m"
#define RESET "\033[0m"
#define MONO 33
#define DEBUG 1
int menu();
void upperCase(char* element);
void upperCaseString(char* missileType);
int validateLengthWidth(int length, int width);
int stringSplit (char **locationRow, char **locationColumn, char* location);
int validateLocation(int row, int column, int length, int width);
int validateDirection(char direction);
int validateShipLength(int shipLngth, int length, int width, char direction, int row, int column);
int validateShipName(char* shipName);
int validateMissileType(char* missile);
void createBoard(char** board, int length, int width);
void addShip(int row, int column, char direction, char* shipName, int shipLength, char** board);
void readFileMissile(char* argv[], int choice);
void readFileBoard(char* argv[], int choice);
struct Node {
	char* value;
	struct Node* nextptr;
};
void push (struct Node *headptr, int len, char* newValue);
void createLinkedList(struct Node* headptr, char* missileType,int len);
void printBoard(int length, int width, char** board);
void GamePlayMode(int length, int width,char* shipName,char* argv[], char** board);
void writeFile(int choice);
