#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "declarations.h"

void GamePlayMode(int length, int width,char* shipName,char* argv[],char** board)
{
	char* locationMissile = (char*)malloc(sizeof(char));
	char* missileRow = (char*)malloc(sizeof(char));
	char* missileColumn;
	char* missileType = (char*)malloc(sizeof(char));
	int row = 0;
	int column = 0;
	int i =0;
	int j=0;
	int destroyed = FALSE;
	struct Node* headptr = NULL;
	struct Node* curr;

	curr = headptr;
	
	do {
 			
		printf("Enter a location for the missile to be launched.\n");
		scanf("%s", locationMissile);

		stringSplit(&missileRow, &missileColumn, locationMissile);
		row = *missileRow - 64;
		column = *missileColumn - 48;
		printf("row: %d column: %d\n", row, column);
		
		if (validateLocation(row, column, length, width) == FALSE)
		{
			printf("The location of the missile entered is incorrect: either outside of board or not as per requirements.\n Location should always follow the format <width><length>; Example: D4\n");
		}
		
		readFileMissile(argv,1);
		missileType = curr -> value;
		curr = curr -> nextptr;
		printf("Missile: %s\n",missileType);

		if (strcmp(missileType,"SINGLE") == 0)
		{
			if (board[column][row] == '#')
			{
				board[column][row] = 'X';
			}
			else 
			{
				board[column][row] = 'O';
			}
		}
		else if (strcmp(missileType, "SPLASH") == 0)
		{
			row = row -1;
			column = column -1;
			for (i=1; i<3; j++)
			{
				for (j=1; j<3; i++)
				{
					if (board[column][row] == '#')
					{
						board[column][row] = 'X';
					}
					else 
					{
						board[column][row] = 'O';
					}
					row++;
				}
				column++;
			}
		}
		else if (strcmp(missileType, "V-LINE") == 0)
		{
			for (column=0; column<length; column++)
			{
				if (board[column][row] == '#')
				{
					board[column][row] = 'X';
				}
				else 
				{
					board[column][row] = 'O';
				}
			}
		}
		else if (strcmp(missileType, "H-LINE") == 0)
		{
			for (row=0; row<width; width++)
			{
				if (board[column][row] == '#')
				{
					board[column][row] = 'X';
				}
				else 
				{
					board[column][row] = 'O';
				}
			}
		}
		else 
		{
			printf("Error while throwing missile.\n");
			exit(0);
		}

		printBoard(length, width,board);

		for(i=1; i<length+1; i++)
		{
			for (j=0; j<width; j++)
			{
				if (board[i][j] == *shipName)
				{
					destroyed = FALSE;
				}
				else 
				{
					destroyed = TRUE;
				}
			}
		}

	}while ((headptr != NULL ) || (destroyed == FALSE));

}


