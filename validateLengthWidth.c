#include <stdio.h>
#include "declarations.h" 

int validateLengthWidth(int length, int width)
{
	int validate = TRUE;

	if ((length == ' ') || (length < 0) || (length > 12))
	{
		validate = FALSE;
	}

	if ((width == ' ') || (width < 0) || (width > 12))
	{
		validate = FALSE;
	}

	return validate;
}
