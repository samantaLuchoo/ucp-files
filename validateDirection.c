#include <stdio.h>
#include "declarations.h"

int validateDirection(char direction)
{
	int validate = 0;
	switch (direction)
	{
		case 'E':
			validate = 1;
			break;
		case 'W':
			validate = 1;
			break;
		case 'N':
			validate = 1;
			break;
		case 'S':
			validate = 1;
			break;
		default:
			validate = 0;
			break;
	}

	return validate;
}
